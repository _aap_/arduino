////////////////////////////////////////////////
// AAP 01.12.2021
//
// arduino source: https://github.com/arduino/ArduinoCore-avr/tree/master/cores/arduino
// Валкодер из мышки : https://www.rlocman.ru/shem/schematics.html?di=35885
///////////////////////////////////////////////
//Пин подключен к SH_CP входу 74HC595
int clockPin = 3;//6;
//Пин подключен к ST_CP входу 74HC595
int latchPin = 4;//7;
//Пин подключен к DS входу 74HC595
int dataPin = 5;//8;

// Пины разрядов цифер
int pins_numbers[4] = {A5, A4, A3, A2};
byte numbers_array[18] = {
  B01111110, B00001100, B10110110, B10011110, // 0 1 2 3
  B11001100, B11011010, B11111010, B00001110, // 4 5 6 7
  B11111110, B11011110, B11101110, B11111000,// 8 9 A b
  B01110010, B10111100, B11110010, B11100010, // C d E F
  B10000000, B11000110 //  - о
};

#define DEBUG_
#define ulong unsigned long
#ifdef DEBUG
#define DD(X,Y)  do { Serial.print(X); Serial.println(Y);} while(0)
#define D(X)   Serial.println(X)
#else
#define DD(X,Y)  ;
#define D(X)   ;

#endif


const byte InterruptPin = 2;
#define HBridge1H 11
#define HBridge1L 9
#define HBridge2H 12
#define HBridge2L 10


const int MmanagmentCountMax = 32;
struct LoopManagment
{
  ulong (*function)(void);
  unsigned long timer;
  unsigned long timerLast = 0;
};
volatile  LoopManagment  _mmanagment[MmanagmentCountMax];
int _mmanagmentCount = 0;
////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
void setup() {
  //устанавливаем режим OUTPUT
  pinMode(latchPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, OUTPUT);
  for (int i = 0; i < 4; i++)
    pinMode(pins_numbers[i], OUTPUT);


  //контакт, к которому подключен датчик Холла, в режим ввода данных с внутренним подтягивающим резистором
  //контакт 2 будет контактом прерывания, при возникновении прерывания будет вызываться функция toggle
  pinMode(InterruptPin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(InterruptPin), hallSensorChange, CHANGE);

  pinMode(HBridge1H, OUTPUT);
  pinMode(HBridge1L, OUTPUT);
  pinMode(HBridge2H, OUTPUT);
  pinMode(HBridge2L, OUTPUT);

  Serial.begin(115200);
  setLoopMmanager(pinDriver, 1000);
  setLoopMmanager(showNum, 2);
  setLoopMmanager(timerWatch, 1000);
  //showNumber(1, 1);
  //showNum();
}
////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
void loop() {
  loopManager();
}
////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////
enum PinDriverState
{
  Idle = 0,
  Stop1,
  North,
  Stop2,
  South,
  Stop3,
  LastState
};
int _pinDriverSetPin = 0;
PinDriverState _pinDriverState = Idle;
int _pinDriverId = 0;
ulong _pinDriverHalfPeriod = 0;
const ulong  PauseTime = 30;

ulong pinDriver()
{
  ulong newTimer = 0L;

  int lev1 = _pinDriverSetPin ? HIGH : LOW;
  int lev2 = _pinDriverSetPin == 0 ? HIGH : LOW;

  if (_pinDriverState > 0)
    DD("_pinDriverState:", _pinDriverState);

  // ВНИМАНИЕ !!!!!
  // ПРАВИТЬ ВНИМАТЕЛЬНО ИНАЧЕ ВСЕ СГОРИТ !!!!
  switch (_pinDriverState)
  {
    case Idle:
      newTimer = 100000;
      lev1 = LOW;
      lev2 = LOW;
      break;
    case Stop1:
      newTimer = PauseTime;
      lev1 = LOW;
      lev2 = LOW;
      break;
    case North:
      newTimer = _pinDriverHalfPeriod;
      _pinDriverSetPin = LOW; //LOW;
      break;
    case Stop2:
      lev1 = LOW;
      lev2 = LOW;
      newTimer = PauseTime;
      break;
    case South:
      newTimer = _pinDriverHalfPeriod;
      _pinDriverSetPin = HIGH;
      break;
    case Stop3:
      lev1 = LOW;
      lev2 = LOW;
      newTimer = PauseTime;
      break;
    case LastState:
      lev1 = LOW;
      lev2 = LOW;
      _pinDriverState = Idle;
      break;
    default:
      D("ERROR");
      printLeft(0x0E);
      return 0;
      break;
  }
  if (_pinDriverState >= LastState ||  _pinDriverState <= Idle)
    _pinDriverState = Idle;
  else {
    _pinDriverState = (PinDriverState)((int)_pinDriverState + 1);
    DD(lev1, lev2);
  }
  // защита от сгорания
  if (lev1 == lev2 && lev1 == 1)
  {
    D("ERROR");
    printLeft(0x0E);
    return 0;
    
  }
  {
    digitalWrite(HBridge1H, lev1);
    if (lev2 == HIGH)
      analogWrite(HBridge1L, 100);
    else
      digitalWrite(HBridge1L, lev2);

    digitalWrite(HBridge2H, lev2);
    if (lev1 == HIGH)
      analogWrite(HBridge2L, 100);
    else
      digitalWrite(HBridge2L, lev1);
  }
  return (ulong)newTimer;
}
////////////////////////////////////////////////////////////////////
ulong _periodTime = 0L;
void hallSensorChange() {
  int hallState = digitalRead(InterruptPin);
  DD("hallSensorChange:", hallState);
  ulong timeIn = micros();
  DD("timeIn:", timeIn);

  static int outCount = 0;

  if (hallState == HIGH )
  {
    ulong period = (timeIn - _periodTime);
    _pinDriverHalfPeriod = period > 500000 ? 200000 : period >> 1;
    _periodTime = timeIn;
    _pinDriverSetPin = HIGH;
    _pinDriverState = Stop1;
    _mmanagment[_pinDriverId].timer = 1;

    if (++outCount > 3*6) {
      outCount =0;
      DD("HalfPeriod:", _pinDriverHalfPeriod);
      DD("_period", period);
      int prd = (9765 / (period>>10));
      DD("prd:", prd);
      printDec(prd); //(int)9765
    }
  }
  //  else {
  //        _period = timeIn - _period;
  //        _pinDriverHalfPeriod = _period > 200000 ? 400000 : _period <<1;
  //        DD("HalfPeriod: ", _pinDriverHalfPeriod);
  //        _pinDriverSetPin = HIGH;
  //        _pinDriverState = Stop1;
  //        _mmanagment[_pinDriverId].timer = 1;
  //  }
  //DD(" hallState :", hallState);
  printLeft( hallState == HIGH ? 17 : 16);
}

///////////////////////////////////////
int _timer = 0;
ulong _lastMillisTimer = 0;
ulong timerWatch()
{
  ulong delta = micros() - _lastMillisTimer;
  _lastMillisTimer = micros();
  //printDec(_timer);
  printLeft( byte(_timer) % 10);
  _timer++;

  return (ulong)0;
}
////////////////////////////////////////////////////////////////////
char _nunToPrint[4] = {0, 0, 0, 0};
////////////////////////////////////////////////////////////////////
inline void printLeft(byte num)
{
  _nunToPrint[0] = num;
}
////////////////////////////////////////////////////////////////////
inline void printDec(int num)
{
  _nunToPrint[3] = byte(num) % 10;
  _nunToPrint[2] = byte(num / 10) % 10;
  _nunToPrint[1] = byte(num / 100) % 10;
  _nunToPrint[0] = byte(num / 1000) % 10;
}

////////////////////////////////////////////////////////////////////
void printHex(int num)
{
  for (int i = 0; i < 4; i++)
  {
    _nunToPrint[3 - i] = (char)(num & 0x0F);
    num = num >> 4;
  }
}
////////////////////////////////////////////////////////////////////
byte _currientDigit = 0;
ulong showNum()
{
  int lastPos = (_currientDigit == 0) ? 3 : _currientDigit - 1;
  digitalWrite(pins_numbers[lastPos], HIGH);
  digitalWrite(latchPin, LOW);
  shiftOut(dataPin, clockPin, MSBFIRST, numbers_array[_nunToPrint[_currientDigit]]);
  digitalWrite(latchPin, HIGH);
  digitalWrite(pins_numbers[_currientDigit], LOW);
  _currientDigit = _currientDigit == 3 ? 0 : _currientDigit + 1;
  return (ulong)0;
}

////////////////////////////////////////////////////////////////////
void showNumber(int numNumber, int number) {

  // зажигаем нужные сегменты
  digitalWrite(latchPin, LOW);
  shiftOut(dataPin, clockPin, MSBFIRST, numbers_array[number]);
  digitalWrite(latchPin, HIGH);

  // включаем нужный разряд(одну из четырех цифр)
  int num_razryad = pins_numbers[numNumber - 1];
  for (int i = 0; i < 4; i++) {
    // выключаем все
    digitalWrite(pins_numbers[i], HIGH);
  }
  // включаем нужную
  digitalWrite(num_razryad, LOW);
  //  delay(5);
}

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
void setLoopMmanager(ulong (*fun)(void), int timer)
{
  if (pinDriver == fun) {
    _pinDriverId = _mmanagmentCount;
    DD("_pinDriverId", _pinDriverId);
  }
  _mmanagment[_mmanagmentCount].function = fun;
  _mmanagment[_mmanagmentCount].timer = (unsigned long)timer * 1000;
  _mmanagmentCount = (_mmanagmentCount >= MmanagmentCountMax ) ? MmanagmentCountMax :  _mmanagmentCount + 1;
}
////////////////////////////////////////////////////////////////////////////
// нужна поправка : через 71 минуту будет сброшен в 0 micros()
inline void loopManager()
{
  volatile  LoopManagment *mmanagment ;
  for (int i = 0; i < _mmanagmentCount; i++) {
    mmanagment = &_mmanagment[i];
    //    if (mmanagment->timer != 9 && mmanagment->timer != 1000000 && mmanagment->timer != 2000)
    //      DD("mmanagment->timer:", mmanagment->timer);
    unsigned long thisTime = micros() ;
    if (thisTime - mmanagment->timerLast  >=  mmanagment->timer )
    {
      mmanagment->timerLast = micros();// - (thisTime - mmanagment->timerLast - mmanagment->timer );
      //    unsigned long timeExecute = micros();
      ulong newTimer = mmanagment->function();
      //      timeExecute = micros() - timeExecute;
      //        D("---");
      if (newTimer > 0) {
        mmanagment->timer = newTimer;
      }

    }
  }
}

////////////////////////////////////////////////////////////////////////////
