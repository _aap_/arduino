////////////////////////////////////////////////
// AAP 01.12.2021
// AAP 28.09.2022 dual H bridge
// ACS724 - Current Senso
// arduino source: https://github.com/arduino/ArduinoCore-avr/tree/master/cores/arduino
// Валкодер из мышки : https://www.rlocman.ru/shem/schematics.html?di=35885
// arduino sourses https://github.com/arduino/ArduinoCore-avr/blob/master/cores/arduino/wiring.c#L65
// ATMega328  http://mypractic.ru/downloads/pdf/ATMega328.pdf
// Меняем частоту ШИМ на ATmega328 https://alexgyver.ru/lessons/pwm-overclock/
// Ускоряем свою Arduino  https://habr.com/ru/post/141442/   http://www.cyber-place.ru/showthread.php?t=550
// https://github.com/pythonista/CyberLib
// pvm timers http://microsin.net/programming/avr/fast-pwm-on-arduino-leonardo.html https://diyi0t.com/arduino-interrupts-and-timed-events/
// https://samou4ka.net/page/tajmer-schetchik-mikrokontrollerov-avr/
// vectora https://tsibrov.blogspot.com/2019/06/arduino-interrupts-part2.html
// Полевеки + драйвера https://electrotransport.ru/ussr/index.php?topic=58449.0#topmsg
// Теория https://engineering-solutions.ru/motorcontrol/dcmotor/
//        http://www.gaw.ru/html.cgi/adv/app/micros/avr/AVR440.htm
///////////////////////////////////////////////
// Необходимые внешнме интерфейсы
// - стартовое направление вращения
// - реверс
// - тормоз сильный / слабый
// - противоугонка
// - вывод оборотов импульсов на БК
// - измерение тока , выклюяение при перегрузке
// -
// -
///////////////////////////////////////////////
// 3000 rpm = 50 rps  ; 2pos/pole * 12 pole * 50rps = 1200 Hz/position ;
// 1200 rpm = 20 rps  ; 2pos/pole * 24 pole * 20rps = 960 Hz/position ;

#include <avr/io.h>
#include <avr/interrupt.h>
#include "TM1637.h"
#include "CyberLib.h"

//////////////////- extend for CyberLib -////////////////////////
#define DWH_(X) D## X ## _High
#define dHigh(X)  DWH_(X)
#define DWL_(X) D## X ## _Low
#define dLow(X)  DWL_(X)
#define DIN_(X) D## X ## _In
#define dIn(X)  DIN_(X)
#define DO_(X) D## X ## _Out
#define dOut(X)  DO_(X)
#define DR_(X) D## X ## _Read
#define dRead(X)  DR_(X)
#define AR_(X)  X ## _Read
#define aRead(X)  AR_(X)
#define clDigitalWrite(X, Y) do{ if(Y==HIGH){dHigh(X);} else{dLow(X);}}while(0)
#define clDigitalRead(X) dRead(X)
#define clAnalogRead(X) aRead(X)


//#define SetOutput(port,bit) D## ## port |= _BV(bit)
//#define SetOutput(port,bit) DDR ## port |= _BV(bit)
//#define SetInput(port,bit) DDR ## port &= ~_BV(bit)
//#define SetBit(port,bit) PORT ## port |= _BV(bit)
//#define ClearBit(port,bit) PORT ## port &= ~_BV(bit)
//#define WritePort(port,bit,value) PORT ## port = (PORT ## port & ~_BV(bit)) | ((value & 1) << bit)
//#define ReadPort(port,bit) (PIN ## port >> bit) & 1
//#define PullUp(port,bit) { SetInput(port,bit); SetBit(port,bit); }
//#define Release(port,bit) { SetInput(port,bit); ClearBit(port,bit); }
////////////////////////////////////////////////////////////////////
#define CLK A5//pins definitions for TM1637 and can be changed to other ports
#define DIO A4
TM1637 tm1637(CLK, DIO);

#define CurrentSensor A0
#define P_POT     A1

#define PWM_ON_TIMER1
#define MAX_PWM 255

#define InterruptPin  2
#define InterruptPin2  3
#define HBridge1H 12
#define HBridge1L 11
#define HBridge2H 8
#define HBridge2L 10

#define HBridge3H 7
#define HBridge3L 9
#define HBridge4H 5
#define HBridge4L 6
volatile bool PwmHBridge1L = false;
volatile bool PwmHBridge2L = false;
volatile bool PwmHBridge3L = false;
volatile bool PwmHBridge4L = false;
#define SetPwm(X,Y)Pwm ## X = Y
#define IsPwm(X)Pwm ## X
////////////////////////////////////////////////////////////////////
#define ulong unsigned long
#define uint unsigned int

#define DEBUG
#ifdef DEBUG
#define DD(X,Y)  do { Serial.print(X);Serial.print(":"); Serial.println(Y);} while(0)
#define D(X)   Serial.println(X)
#define DV(X)   do { Serial.print(#X);Serial.print(":"); Serial.println(X);} while(0)
#else
#define DD(X,Y)  ;
#define D(X)   ;
#define DV(X)  ;
#endif

////////////////////////////////////////////////////////////////////

const int MmanagmentCountMax = 32;
int PotentiometerNil = 300;
volatile int PotentiometerPwm = 0;
//volatile uint HallSensorChangeWatchdog(0);
////////////////////////////////////////////////////////////////////
struct LoopManagment
{
  ulong (*function)(void);
  unsigned long timer;
  unsigned long timerLast = 0;
};
volatile  LoopManagment  _mmanagment[MmanagmentCountMax];
int _mmanagmentCount = 0;
////////////////////////////////////////////////////////////////////
#define CORRECT_CLOCK 3
#define micros() (micros() >> CORRECT_CLOCK)
#define millis() (millis() >> CORRECT_CLOCK)
////////////////////////////////////////////////////////////////////
//
void setup() {

  //  dOut(HBridge1H);
  //  dOut(HBridge1L);
  //  dOut(HBridge2H);
  //  dOut(HBridge2L);
  //  dOut(HBridge3H);
  //  dOut(HBridge3L);
  //  dOut(HBridge4H);
  //  dOut(HBridge4L);
  //
  //  clDigitalWrite(HBridge1H, LOW);
  //  clDigitalWrite(HBridge1L, LOW);
  //  clDigitalWrite(HBridge2H, LOW);
  //  clDigitalWrite(HBridge2L, LOW);
  //  clDigitalWrite(HBridge3H, LOW);
  //  clDigitalWrite(HBridge3L, LOW);
  //  clDigitalWrite(HBridge4H, LOW);
  //  clDigitalWrite(HBridge4L, LOW);

  pinMode(HBridge1H, OUTPUT);
  pinMode(HBridge1L, OUTPUT);
  pinMode(HBridge2H, OUTPUT);
  pinMode(HBridge2L, OUTPUT);
  pinMode(HBridge3H, OUTPUT);
  pinMode(HBridge3L, OUTPUT);
  pinMode(HBridge4H, OUTPUT);
  pinMode(HBridge4L, OUTPUT);
  digitalWrite(HBridge1H, LOW);
  digitalWrite(HBridge1L, LOW);
  digitalWrite(HBridge2H, LOW);
  digitalWrite(HBridge2L, LOW);
  digitalWrite(HBridge3H, LOW);
  digitalWrite(HBridge3L, LOW);
  digitalWrite(HBridge4H, LOW);
  digitalWrite(HBridge4L, LOW);

  tm1637.init();
  tm1637.set(BRIGHT_DARKEST);//BRIGHT_TYPICAL = 2,BRIGHT_DARKEST = 0,BRIG

  //контакт, к которому подключен датчик Холла, в режим ввода данных с внутренним подтягивающим резистором
  //контакт 2 будет контактом прерывания, при возникновении прерывания будет вызываться функция toggle
  pinMode(InterruptPin, INPUT_PULLUP);
  pinMode(InterruptPin2, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(InterruptPin), hallSensorChange, CHANGE  );//CHANGE
  attachInterrupt(digitalPinToInterrupt(InterruptPin2), hallSensorChange2, CHANGE  );//CHANGE

  // Пины D5 и D6 - 7.8 кГц
  TCCR0B = 0b00000010;  // x8
  TCCR0A = 0b00000011;  // fast pwm
  // Пины D9 и D10 - 7.8 кГц
  TCCR1A = 0b00000001;  // 8bit
  TCCR1B = 0b00001010;  // x8 fast pwm
  // Пины D3 и D11 - 8 кГц
  TCCR2B = 0b00000010;  // x8
  TCCR2A = 0b00000011;  // fast pwm
#ifdef DEBUG
  Serial.begin(115200);
#endif
#ifndef _PWM_ON_TIMER1_
  setLoopMmanager(pinDriver, 1000);
#endif
  setLoopMmanager(showNum, 100);
  setLoopMmanager(currienMesure, 10);
  setLoopMmanager(potentiometer, 10);
  //setLoopMmanager(watchdog, 100);
  //setLoopMmanager(timerWatch, 1000);
  //showNumber(1, 1);
  //showNum();
  setStatePin(false);
  PotentiometerNil = potentiometerNil();
  PotentiometerNil += PotentiometerNil / 10; // + 10%
  doStep(false);
#ifdef PWM_ON_TIMER1
  StartTimer1();
  pinMode(4, OUTPUT);
#endif
}


////////////////////////////////////////////////////////////////////
//
////////////////////////////////////////////////////////////////////
void loop() {
  loopManager();
}
////////////////////////////////////////////////////////////////////
//
////////////////////////////////////////////////////////////////////


////////////////////////////////
void StartTimer1()
{
  TIMSK1 &= ~(1 << TOIE1); //запретить прерывания по таймеру1
  TCCR1A = 0;   //timer1 off
  TCCR1B = 0;   //prescaler off (1<<CTC1)-3й бит
  TCCR1B |= (1 << CS10);  // no div
  //TCCR1B |= (1 << CS11);  // div 8
  TCNT1 = 0;//dub_tcnt1;  // выставляем начальное значение TCNT1
  TIMSK1 |= (1 << TOIE1); // разрешаем прерывание по переполнению таймера
  sei(); // включить глобальные прерывания
}
///////////////////////////////////////////////////////////////
#define DIV_BIT 5 // регулировка частоты ШИМ
ISR(TIMER1_OVF_vect)
{
  static bool side(true);
  side =  !side ;
  volatile byte pwm ( PotentiometerPwm); // PotentiometerPwm - может меняться в процессе исполнения ?
  if (side) {
    if (pwm)
    {
      clDigitalWrite(4, HIGH );
      if ( !IsPwm(HBridge1L))    clDigitalWrite(HBridge1L, LOW);
      if ( !IsPwm(HBridge2L))    clDigitalWrite(HBridge2L, LOW);
      if ( !IsPwm(HBridge3L))    clDigitalWrite(HBridge3L, LOW);
      if ( !IsPwm(HBridge4L))    clDigitalWrite(HBridge4L, LOW);
      if ( IsPwm(HBridge1L))    clDigitalWrite(HBridge1L, HIGH);
      if ( IsPwm(HBridge2L))    clDigitalWrite(HBridge2L, HIGH);
      if ( IsPwm(HBridge3L))    clDigitalWrite(HBridge3L, HIGH);
      if ( IsPwm(HBridge4L))    clDigitalWrite(HBridge4L, HIGH);
      TCNT1 = 65535 - (pwm << DIV_BIT);
    }
  } else
  {
    TCNT1 = 65535 - ((MAX_PWM << DIV_BIT)  - (pwm << DIV_BIT));
    if (  pwm < (MAX_PWM )) {
      clDigitalWrite(4, LOW );
      clDigitalWrite(HBridge1L, LOW);
      clDigitalWrite(HBridge2L, LOW);
      clDigitalWrite(HBridge3L, LOW);
      clDigitalWrite(HBridge4L, LOW);
    }
  }
}
////////////////////////////////////////////////////////////////////
inline void PauseTimer1(void)
{
  TIMSK1 &= ~(1 << TOIE1);  //запретить прерывания по таймеру1
}
////////////////////////////////////////////////////////////////////
inline void ResumeTimer1(void)
{
  TIMSK1 |= (1 << TOIE1); //Продолжить отсчет, (разрешить прерывания по таймеру1)
}


////////////////////////////////////////////////////////////////////
//ulong watchdog()
//{
//  // if (HallSensorChangeWatchdog)
//  ulong newTimer (0L);
//  return newTimer ;
//}


////////////////////////////////////////////////////////////////////
enum PinDriverState
{
  Idle = 0,
  Stop,
  Poll,
  Step,
  Pause,
  LastState
};
int _pinDriverId = 0;
const ulong  _pinDriverHalfPeriodReset = 150000;
ulong _pinDriverHalfPeriod = _pinDriverHalfPeriodReset;
const ulong  TimePause = 4;
const ulong  TimePoll = 100;
const int StateSize = 8;

//int leftState1[StateSize] = {2, 2, 0, 0, 1, 1, 0, 0 };
//int leftState2[StateSize] = {0, 0, 1, 1, 0, 0, 2, 2};
//int leftState1[StateSize] = {1, 1, 1, 1, 2, 2, 2, 2};
//int leftState2[StateSize] = {2, 2, 1, 1, 1, 1, 2, 2 };
//int leftState1[StateSize] = {0, 0, 0, 0, 0, 0, 0, 0};
//int leftState2[StateSize] = {2, 2, 2, 2, 2, 2, 2, 2};
//int leftPause1[StateSize] = {1, 1};
//int leftPause2[StateSize] = {1, 1};
//int rightState1[StateSize] = {0, 0, 0, 0, 1, 1, 1, 1};
//int rightState2[StateSize] = {0, 0, 1, 1, 1, 1, 0, 0};
volatile int _statePin = 0;
volatile int _StatePin1 (0), _StatePin2(0);
volatile bool IsBoost(false);
//volatile PinDriverState _pinDriverState = Poll;//Stop;//Idle;

////////////////////////////////////////////////////////////////////
inline bool setStatePin(bool isHall )
{
  static int hallStateLast (-1), hallStateLast2 (-1);
  static bool lastResult(true); /// показывает крутится ли мотор
  int hallState = clDigitalRead(InterruptPin);
  int hallState2 = clDigitalRead(InterruptPin2);
  bool result(hallStateLast != hallState || hallStateLast2 != hallState2);
  hallStateLast = hallState ; hallStateLast2 = hallState2;
  static int rpmnow(0);
  if (result && isHall)
    rpmnow = rpm();
  //Угол опережения фаз (timing)
  //  IsBoost = (rpmnow > 11000);
  //  if (hallState == HIGH )
  //  {
  //    if (hallState2 == HIGH )
  //      _statePin = IsBoost ? 2 : 0;
  //    else
  //      _statePin = IsBoost ? 4 : 2;
  //  }  else {
  //    if (hallState2 == HIGH )
  //      _statePin = IsBoost ? 0 : 6;
  //    else
  //      _statePin = IsBoost ? 6 : 4;
  //  }

  _StatePin2 = hallState;
  _StatePin1 = hallState2;
  DV(hallState);
  DV(hallState2);
//  if ( rpmnow > 1500)
//  {
//    _StatePin2 = hallState == 1 ? 1 : -1;
//    _StatePin1 = hallState2 == 0 ? 0 : -1;
//  }

  if ( rpmnow < 200) {
    printLeft( hallState == HIGH ? 1 : 8);
    printSecond( hallState2 == HIGH ? 1 : 8);
  }

  if (!isHall ) {
    DV(lastResult);
    result = lastResult;
    lastResult = false;
  } else
    lastResult = isHall && result;

  if (result)
    doStep(true);
  return result ;
}
////////////////////////////////////////////////////////////////////
//
ulong pinDriver()
{
  static PinDriverState _pinDriverState = Stop;//Idle;
  static bool laststop;
  ulong newTimer = 0L;

  // ВНИМАНИЕ !!!!!
  // ПРАВИТЬ ВНИМАТЕЛЬНО ИНАЧЕ ВСЕ СГОРИТ !!!!
  switch (_pinDriverState)
  {
    case Idle:
      DD("_pinDriverState:", _pinDriverState);
      newTimer = 100000;
      D("Idle");
      _pinDriverState = Idle;
      break;
    case Stop:
      newTimer = TimePause;
      DD("Stop", newTimer);
      doStep(false);
      _pinDriverState = Step;
      break;
    case Poll:
      newTimer = TimePoll;
      if (PotentiometerPwm && !laststop) {
        DD("Poll", newTimer);
        DV(laststop);
        _pinDriverState = Stop;
        laststop = PotentiometerPwm;
        break;
      }
      if (!PotentiometerPwm  ) { //&& laststop
        doStep(false);
      }
      laststop = PotentiometerPwm;
      break;
    case Step : {
        // без датчиковый режим     newTimer = _pinDriverHalfPeriodReset * 300 / (255 - PotentiometerPwm) ; //_pinDriverHalfPeriod;
        _pinDriverState = Poll;
        newTimer = _pinDriverHalfPeriodReset;
        if (PotentiometerPwm && !setStatePin(false)) {
          DD("Step", newTimer);
          _pinDriverState = Step;
          doStep(true);
        }
        else
          DD("Step->poll", newTimer);
      }
      break;
    case Pause:
      newTimer = TimePause;
      _pinDriverState = Stop;
      break;
    case LastState:
      _pinDriverState = Stop;
      break;
    default:
      D("ERROR");
      printLeft(0x0E);
      _pinDriverState = Idle;
      return 0;
      break;
  }
  return (ulong)newTimer;
}

////////////////////////////////////////////////////////////////////
//
inline void doStep(bool set)
{
  int lev1(LOW);
  int lev2(LOW);
  int lev3(LOW);
  int lev4(LOW);

  //  lev1 = leftState1[_statePin] == 2 ? HIGH : LOW;
  //  lev2 = leftState1[_statePin] == 1 ? HIGH : LOW;
  //  lev3 = leftState2[_statePin] == 2 ? HIGH : LOW;
  //  lev4 = leftState2[_statePin] == 1 ? HIGH : LOW;


  lev1 = _StatePin1 == 1 ? HIGH : LOW;
  lev2 = _StatePin1 == 0 ? HIGH : LOW;
  lev3 = _StatePin2 == 1 ? HIGH : LOW;
  lev4 = _StatePin2 == 0 ? HIGH : LOW;


  if (!set) {
    //D("doStep reset!");
    lev1 = LOW;
    lev2 = LOW;
    lev3 = LOW;
    lev4 = LOW;
  }
  // защита от сгорания
  if ((lev1 == lev2 && lev1 == HIGH) || (lev3 == lev4 && lev3 == HIGH))
  {
    D("ERROR");
    printLeft(0x0E);
    return ;
  }
  byte pwm( PotentiometerPwm);
#ifdef PWM_ON_TIMER1
  ///PauseTimer1();
  //D("doStep PWM_ON_TIMER1");
  //Для начала выключим , что надо выключить
  if (lev1 == LOW) {
    clDigitalWrite(HBridge1H, LOW);
    SetPwm(HBridge2L, false );
    clDigitalWrite(HBridge2L, LOW);
  }
  if (lev2 == LOW) {
    clDigitalWrite(HBridge2H, LOW);
    SetPwm(HBridge1L, false );
    clDigitalWrite(HBridge1L, LOW);
  }
  if (lev3 == LOW) {
    clDigitalWrite(HBridge3H, LOW);
    SetPwm(HBridge4L, false );
    clDigitalWrite(HBridge4L, LOW);
  }
  if (lev4 == LOW) {
    clDigitalWrite(HBridge4H, LOW);
    SetPwm(HBridge3L, false );
    clDigitalWrite(HBridge3L, LOW);
  }
  //Теперь включим , что надо включить
  clDigitalWrite(HBridge1H, lev1);
  clDigitalWrite(HBridge2H, lev2);
  SetPwm(HBridge1L, lev2 == HIGH );
  SetPwm(HBridge2L, lev1 == HIGH );

  clDigitalWrite(HBridge3H, lev3);
  clDigitalWrite(HBridge4H, lev4);
  SetPwm(HBridge3L, lev4 == HIGH );
  SetPwm(HBridge4L, lev3 == HIGH );

  // ResumeTimer1();
#else
  //PWM_ON_TIMERS 0 1 2
  //Для начала выключим , что надо выключить
  if (lev1 == LOW) {
    clDigitalWrite(HBridge1H, lev1);
    analogWrite(HBridge2L, lev1);
  }
  if (lev2 == LOW) {
    clDigitalWrite(HBridge2H, LOW);
    analogWrite(HBridge1L, LOW);
  }
  if (lev3 == LOW) {
    clDigitalWrite(HBridge3H, LOW);
    analogWrite(HBridge4L, LOW);
  }
  if (lev4 == LOW) {
    clDigitalWrite(HBridge4H, LOW);
    analogWrite(HBridge3L, LOW);
  }
  clDigitalWrite(HBridge1H, lev1);
  analogWrite(HBridge1L, lev2 == HIGH ? pwm : 0);
  clDigitalWrite(HBridge2H, lev2);
  analogWrite(HBridge2L, lev1 == HIGH ? pwm : 0);
  clDigitalWrite(HBridge3H, lev3);
  analogWrite(HBridge3L, lev4 == HIGH ? pwm : 0);
  clDigitalWrite(HBridge4H, lev4);
  analogWrite(HBridge4L, lev3 == HIGH ? pwm : 0);
#endif

}

////////////////////////////////////////////////////////////////////
//
inline void hallSensorChange() {
  setStatePin(true);
}
inline void hallSensorChange2() {
  setStatePin(true);
}
////////////////////////////////////////////////////////////////////
//
inline int rpm() {
  static  ulong _periodTime = 0L;
  static int outCount = 0;
  static ulong turnTime = 0L;
  static int prd(0);

  ulong timeIn = micros();
  ulong period = (timeIn - _periodTime);
  _periodTime = timeIn;
  turnTime += period;
  if (++outCount > 24 ) {
    //int prd = (9765 / (period >> 10));
    prd = (60 * 1000000 / turnTime);
    //    DD("prd", prd);
    printDec(prd);
    outCount = 0;
    turnTime = 0;
  }
  return prd;
}

///////////////////////////////////////
int _timer = 0;
ulong _lastMillisTimer = 0;
ulong timerWatch()
{
  ulong delta = micros() - _lastMillisTimer;
  _lastMillisTimer = micros();
  printDec(_timer);
  //printLeft( byte(_timer) % 10);
  _timer++;

  return (ulong)0;
}
////////////////////////////////////////////////////////////////////
//
#define NullMesure 490
uint mVoltageReferenceHalf = 2100;
uint mV_A = 40;
uint ma_mv =  25;

ulong currienMesure()
{
  static int i(0);
  static ulong sV(0);
  int V = analogRead(CurrentSensor);
  sV += V;

  if (++i >= 8) {
    //DD(V, sV);
    sV >>= 3;
    //DV(sV);
    //sV=sV/20;
    uint A = sV - NullMesure;
    int aaa = (ulong)(mVoltageReferenceHalf * ma_mv * A) / (NullMesure * 10) ;
    //DV(aaa);
    //printDec(aaa);
    sV = i = 0;
  }
  return (ulong)0;
}
////////////////////////////////////////////////////////////////////
int potentiometerNil()
{
  ulong sV(0);
  for (int i = 0; i < 16; ++i) {
    int V = clAnalogRead(P_POT);
    sV += V;
  }
  sV >>= 4;
  DD("potentiometerNil", sV);
  return sV;
}
////////////////////////////////////////////////////////////////////
ulong potentiometer()
{
  static int i(0);
  static ulong sV(0);
  int V = clAnalogRead(P_POT);
  sV += V;

  if (++i >= 8) {
    int potentiometer(PotentiometerPwm);
    //DD(V, sV);
    sV >>= 3;
    potentiometer = sV - PotentiometerNil;
    potentiometer = potentiometer < 0 ? 0 : potentiometer;
    //DD(">", potentiometer);
    potentiometer >>= 1;
    // potentiometer = potentiometer < 200 ? potentiometer >>1 : (200>>1) + ( potentiometer-200 );
    potentiometer = potentiometer > MAX_PWM ? MAX_PWM : potentiometer;
    if (potentiometer)      D(potentiometer);
    sV = i = 0;
    PotentiometerPwm = potentiometer;
  }
  return (ulong)0;
}

////////////////////////////////////////////////////////////////////
char _nunToPrint[4] = {0, 0, 0, 0};
////////////////////////////////////////////////////////////////////
inline void printLeft(byte num)
{
  _nunToPrint[0] = num;
}
////////////////////////////////////////////////////////////////////
inline void printSecond(byte num)
{
  _nunToPrint[1] = num;
}
////////////////////////////////////////////////////////////////////
inline void printDec(int out)
{
  int num = out;
  for (int i = 3; i >= 0; --i) {
    _nunToPrint[i] = num % 10;
    num = num / 10;
  }
}

////////////////////////////////////////////////////////////////////
void printHex(int num)
{
  for (int i = 0; i < 4; i++)  {
    _nunToPrint[3 - i] = (char)(num & 0x0F);
    num = num >> 4;
  }
}
////////////////////////////////////////////////////////////////////
byte _currientDigit = 0;
ulong showNum()
{
  //  for (int i = 0; i < 4; ++i)
  //    tm1637.display(i, _nunToPrint[i]);
  tm1637.display(_currientDigit, _nunToPrint[_currientDigit]);
  _currientDigit = _currientDigit == 3 ? 0 : _currientDigit + 1;
  return (ulong)0;
}


////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
void setLoopMmanager(ulong (*fun)(void), int timer)
{
  if (pinDriver == fun) {
    _pinDriverId = _mmanagmentCount;
    DD("_pinDriverId", _pinDriverId);
  }
  _mmanagment[_mmanagmentCount].function = fun;
  _mmanagment[_mmanagmentCount].timer = (unsigned long)timer * 1000;
  _mmanagmentCount = (_mmanagmentCount >= MmanagmentCountMax ) ? MmanagmentCountMax :  _mmanagmentCount + 1;
}
////////////////////////////////////////////////////////////////////////////
// нужна поправка : через 71 минуту будет сброшен в 0 micros()
inline void loopManager()
{
  volatile  LoopManagment *mmanagment ;
  for (int i = 0; i < _mmanagmentCount; i++)
  {
    mmanagment = &_mmanagment[i];
    //    if (mmanagment->timer != 9 && mmanagment->timer != 1000000 && mmanagment->timer != 2000)
    //      DD("mmanagment->timer:", mmanagment->timer);
    unsigned long thisTime = micros() ;
    if (thisTime - mmanagment->timerLast  >=  mmanagment->timer )
    {
      mmanagment->timerLast = micros();// - (thisTime - mmanagment->timerLast - mmanagment->timer );
      //    unsigned long timeExecute = micros();
      ulong newTimer = mmanagment->function();
      //      timeExecute = micros() - timeExecute;
      //        D("---");
      if (newTimer > 0) {
        mmanagment->timer = newTimer;
      }

    }
  }
}

////////////////////////////////////////////////////////////////////////////
