////////////////////////////////////////////////
// AAP 01.12.2021
// AAP 28.09.2022 dual H bridge
// AAP 17.01.2023 dual H bridge Async
// AAP 10.03.2024 dual H bridge Ferro
// ACS724 - Current Senso
// arduino source: https://github.com/arduino/ArduinoCore-avr/tree/master/cores/arduino
// Валкодер из мышки : https://www.rlocman.ru/shem/schematics.html?di=35885
// arduino sourses https://github.com/arduino/ArduinoCore-avr/blob/master/cores/arduino/wiring.c#L65
// ATMega328  http://mypractic.ru/downloads/pdf/ATMega328.pdf
// Меняем частоту ШИМ на ATmega328 https://alexgyver.ru/lessons/pwm-overclock/
// Ускоряем свою Arduino  https://habr.com/ru/post/141442/   http://www.cyber-place.ru/showthread.php?t=550
// https://github.com/pythonista/CyberLib
// pvm timers http://microsin.net/programming/avr/fast-pwm-on-arduino-leonardo.html https://diyi0t.com/arduino-interrupts-and-timed-events/
// https://samou4ka.net/page/tajmer-schetchik-mikrokontrollerov-avr/
// vectora https://tsibrov.blogspot.com/2019/06/arduino-interrupts-part2.html
// Полевеки + драйвера https://electrotransport.ru/ussr/index.php?topic=58449.0#topmsg
// Теория https://engineering-solutions.ru/motorcontrol/dcmotor/
//        http://www.gaw.ru/html.cgi/adv/app/micros/avr/AVR440.htm
// замер двигателя в гору https://electrotransport.ru/ussr/index.php?msg=1554921   https://ebikes.ca/tools/simulator.html
// https://radioschema.ru/el-komponenty/tranzistory/kp731-kp771.html
//   КП747А   100 41 230
//   КП771А   100 40 150
// https://wiki.roboforum.ru/index.php?title=H-%D0%BC%D0%BE%D1%81%D1%82
//Параллельное включение разных MOSFET (IRFP4468 и IRFP4110) https://www.radiokot.ru/forum/viewtopic.php?f=11&t=142614
// Патент https://electrotransport.ru/ussr/index.php?topic=23140.0#topmsg
//STM32F частотный преобразователь для трёхфазного асинхронного двигателя. http://progcont.ru/?articles=45&category_articles=STM32F
//TC4420 - 6 A MOSFET Gate Driver
// arduino china https://mcudude.github.io/MiniCore/package_MCUdude_MiniCore_index.json
//https://en.wikipedia.org/wiki/Switched_reluctance_motor
///////////////////////////////////////////////
// IRFB4110 STP40N10 https://www.lcsc.com/search?q=IRFb4110
// кп771а
///////////////////////////////////////////////
// Необходимые внешнме интерфейсы
// - стартовое направление вращения
// - реверс
// - тормоз сильный / слабый
// - противоугонка
// - вывод оборотов импульсов на БК
// - измерение тока , выклюяение при перегрузке
// -
// -
///////////////////////////////////////////////
// 3000 rpm = 50 rps  ; 1pos/pole * 12 pole * 50rps = 600 Hz/position ;
// 1200 rpm = 20 rps  ; 2pos/pole * 24 pole * 20rps = 960 Hz/position ;

#define UseTM1637
#define DEBUG

#include <Arduino.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#ifdef UseTM1637
#include "TM1637.h"
#endif
#include "CyberLib.h"

//////////////////- extend for CyberLib -////////////////////////
#define DWH_(X) D##X##_High
#define dHigh(X) DWH_(X)
#define DWL_(X) D##X##_Low
#define dLow(X) DWL_(X)
#define DIN_(X) D##X##_In
#define dIn(X) DIN_(X)
#define DO_(X) D##X##_Out
#define dOut(X) DO_(X)
#define DR_(X) D##X##_Read
#define dRead(X) DR_(X)
#define AR_(X) X##_Read
#define aRead(X) AR_(X)
#define clDigitalWrite(X, Y) \
  do { \
    if (Y == HIGH) { \
      dHigh(X); \
    } else { \
      dLow(X); \
    } \
  } while (0)
#define clDigitalRead(X) dRead(X)
#define clAnalogRead(X) aRead(X)


//#define SetOutput(port,bit) D## ## port |= _BV(bit)
//#define SetOutput(port,bit) DDR ## port |= _BV(bit)
//#define SetInput(port,bit) DDR ## port &= ~_BV(bit)
//#define SetBit(port,bit) PORT ## port |= _BV(bit)
//#define ClearBit(port,bit) PORT ## port &= ~_BV(bit)
//#define WritePort(port,bit,value) PORT ## port = (PORT ## port & ~_BV(bit)) | ((value & 1) << bit)
//#define ReadPort(port,bit) (PIN ## port >> bit) & 1
//#define PullUp(port,bit) { SetInput(port,bit); SetBit(port,bit); }
//#define Release(port,bit) { SetInput(port,bit); ClearBit(port,bit); }
////////////////////////////////////////////////////////////////////
#define CLK A5  //pins definitions for TM1637 and can be changed to other ports
#define DIO A4

#define CurrentSensor A0
#define P_POT A1

// #define InterruptPin 2
// #define InterruptPin2 3

#define LED 13

#define HBridge1H 9
#define HBridge1L 11
#define HBridge2H 10
#define HBridge2L 12

#define HBridge3H 5
#define HBridge3L 7
#define HBridge4H 6
#define HBridge4L 8
////////////////////////////////////////////////////////////////////
#define MAX_PWM 255

#ifdef UseTM1637
TM1637 tm1637(CLK, DIO);
#endif
////////////////////////////////////////////////////////////////////
volatile bool PwmHBridge1L = false;
volatile bool PwmHBridge2L = false;
volatile bool PwmHBridge3L = false;
volatile bool PwmHBridge4L = false;
#define SetPwm(X, Y) Pwm##X = Y
#define IsPwm(X) Pwm##X
////////////////////////////////////////////////////////////////////
#define ulong unsigned long
#define uint unsigned int


#ifdef DEBUG
#define DD(X, Y) \
  do { \
    Serial.print(X); \
    Serial.print(":"); \
    Serial.println(Y); \
  } while (0)
#define D(X) Serial.println(X)
#define DV(X) \
  do { \
    Serial.print(#X); \
    Serial.print(":"); \
    Serial.println(X); \
  } while (0)
#define W(X) \
  do { \
    Serial.print("ERROR:"); \
    Serial.println(X); \
  } while (0)
#else
#define DD(X, Y) ;
#define D(X) ;
#define DV(X) ;
#define W(X) ;
#endif

////////////////////////////////////////////////////////////////////

const int MmanagmentCountMax = 32;
int PotentiometerNil = 300;
volatile int PotentiometerPwm = 0;
//volatile uint HallSensorChangeWatchdog(0);
int getPotentiometerNil();
void doStep(bool set);
ulong potentiometer();
ulong pinDriver();
ulong showDec();
////////////////////////////////////////////////////////////////////
struct LoopManagment {
  ulong (*function)(void);
  unsigned long timer;
  unsigned long timerLast = 0;
};
volatile LoopManagment _mmanagment[MmanagmentCountMax];
int _mmanagmentCount = 0;
////////////////////////////////////////////////////////////////////
#define CORRECT_CLOCK 3
#define micros() (micros() >> CORRECT_CLOCK)
#define millis() (millis() >> CORRECT_CLOCK)
////////////////////////////////////////////////////////////////////
//
void setup() {
  pinMode(HBridge1H, OUTPUT);
  pinMode(HBridge1L, OUTPUT);
  pinMode(HBridge2H, OUTPUT);
  pinMode(HBridge2L, OUTPUT);
  pinMode(HBridge3H, OUTPUT);
  pinMode(HBridge3L, OUTPUT);
  pinMode(HBridge4H, OUTPUT);
  pinMode(HBridge4L, OUTPUT);
  digitalWrite(HBridge1H, LOW);
  digitalWrite(HBridge1L, LOW);
  digitalWrite(HBridge2H, LOW);
  digitalWrite(HBridge2L, LOW);
  digitalWrite(HBridge3H, LOW);
  digitalWrite(HBridge3L, LOW);
  digitalWrite(HBridge4H, LOW);
  digitalWrite(HBridge4L, LOW);
#ifdef UseTM1637
  tm1637.init();
  tm1637.set(BRIGHT_DARKEST);  //BRIGHT_TYPICAL = 2,BRIGHT_DARKEST = 0,BRIG
#endif

  //контакт, к которому подключен датчик Холла, в режим ввода данных с внутренним подтягивающим резистором
  //контакт 2 будет контактом прерывания, при возникновении прерывания будет вызываться функция toggle
  // pinMode(InterruptPin, INPUT_PULLUP);
  // pinMode(InterruptPin2, INPUT_PULLUP);
  // attachInterrupt(digitalPinToInterrupt(InterruptPin), hallSensorChange, CHANGE);    //CHANGE
  // attachInterrupt(digitalPinToInterrupt(InterruptPin2), hallSensorChange2, CHANGE);  //CHANGE

  // Пины D5 и D6 - 7.8 кГц
  TCCR0B = 0b00000010;  // x8
  TCCR0A = 0b00000011;  // fast pwm
  // Пины D9 и D10 - 7.8 кГц
  TCCR1A = 0b00000001;  // 8bit
  TCCR1B = 0b00001010;  // x8 fast pwm
  // Пины D3 и D11 - 8 кГц
  TCCR2B = 0b00000010;  // x8
  TCCR2A = 0b00000011;  // fast pwm
#ifdef DEBUG
  Serial.begin(115200);
#endif
  // setLoopMmanager(pinDriver, 1000);
  // //setLoopMmanager(showNum, 100);
  // setLoopMmanager(showDec, 250);
  // //setLoopMmanager(currienMesure, 10);
  // setLoopMmanager(potentiometer, 10);
  // //setLoopMmanager(watchdog, 100);
  // //setLoopMmanager(timerWatch, 1000);
  //showNumber(1, 1);
  //showNum();
  setStatePin(0);
  PotentiometerNil = getPotentiometerNil();
  PotentiometerNil += PotentiometerNil / 10;  // + 10%
  doStep(false);
}


////////////////////////////////////////////////////////////////////
//
////////////////////////////////////////////////////////////////////
void loop() {
  static uint loopControl = 0;
  potentiometer();
  //pinDriver();
  swing();
  if ((++loopControl & 3) == 0)
    showDec();
  delay(10);
  //   printLeft(hallState == HIGH ? 1 : 8);
}
////////////////////////////////////////////////////////////////////
//
////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////
enum PinDriverState {
  Idle = 0,
  Stop,
  Poll,
  Step,
  Pause,
  LastState
};
int _pinDriverId = 0;
const ulong _pinDriverHalfPeriodReset = 15000;
ulong _pinDriverHalfPeriod = _pinDriverHalfPeriodReset;
const ulong TimePause = 4;
const ulong TimePoll = 100;
const int StateSize = 8;
int _decToPrint(0);

volatile int _StatePin1(0), _StatePin2(0);
volatile bool IsBoost(false);
//volatile PinDriverState _pinDriverState = Poll;//Stop;//Idle;
volatile int RpmNow(0);

////////////////////////////////////////////////////////////////////
inline bool setStatePin(int hall) {
  static int hallStateLast(-1), hallStateLast2(-1);
  static bool lastResult(false);  /// показывает крутится ли мотор
  static ulong lastMillisTimer[3];
  static ulong deltaMillis[3] = { 0, 0, 0 };
  static int lastHall(0);

  // int hallState = clDigitalRead(InterruptPin);
  // int hallState2 = clDigitalRead(InterruptPin2);
  // bool result(hallStateLast != hallState || hallStateLast2 != hallState2);
  // hallStateLast = hallState;
  // hallStateLast2 = hallState2;


  // if (hall > 0) {
  //   ulong currMills(micros());
  //   ulong delta = currMills - lastMillisTimer[hall];
  //   lastMillisTimer[hall] = currMills;

  //   if (hall != lastHall && result) {
  //     lastHall = hall;
  //     D(delta);
  //   } else {
  //     if (delta > deltaMillis[hall])
  //       deltaMillis[hall] = delta;
  //     D("---");
  //     DD(delta, hall);
  //     DD(hallState, hallState2);

  //     return false;
  //   }
  //   // // if (deltaMillis[hall] > 1000 && delta < 30000) {
  // //   DD("reurn",hall);
  // //   DV(delta);
  // //   return false;
  // // }
  // if (delta > deltaMillis[hall])
  //   deltaMillis[hall] = delta;
  // DD(delta, hall);
  // DD(hallState, hallState2);
  // }

  // if (result && hall)
  //   RpmNow = rpm();
  //Угол опережения фаз (timing)
  //  IsBoost = (rpmnow > 11000);
  //  if (hallState == HIGH )
  //  {
  //    if (hallState2 == HIGH )
  //      _statePin = IsBoost ? 2 : 0;
  //    else
  //      _statePin = IsBoost ? 4 : 2;
  //  }  else {
  //    if (hallState2 == HIGH )
  //      _statePin = IsBoost ? 0 : 6;
  //    else
  //      _statePin = IsBoost ? 6 : 4;
  //  }

  // _StatePin2 = 0;
  // _StatePin1 = 0;

  // if (RpmNow < 100) {
  //   printLeft(hallState == HIGH ? 1 : 8);
  //   printSecond(hallState2 == HIGH ? 1 : 8);
  // }

  // if (!hall) {
  //   result = lastResult;
  //   lastResult = false;
  // } else
  //   lastResult = hall && result;

  // if (result) {
  //   if (hall == 1)
  //     doStep1(true);
  //   if (hall == 2)
  //     doStep2(true);
  // }
  return false;
}


////////////////////////////////////////////////////////////////////
//
ulong pinDriver() {
  static PinDriverState _pinDriverState = Stop;  //Idle;
  static bool laststop(false);
  ulong newTimer = 0L;

  // ВНИМАНИЕ !!!!!
  // ПРАВИТЬ ВНИМАТЕЛЬНО ИНАЧЕ ВСЕ СГОРИТ !!!!
  switch (_pinDriverState) {
    case Idle:
      DD("_pinDriverState:", _pinDriverState);
      newTimer = 100000;
      D("Idle");
      _pinDriverState = Idle;
      break;
    case Stop:
      newTimer = TimePause;
      DD("Stop", newTimer);
      doStep(false);
      _pinDriverState = Poll;
      break;
    case Poll:
      newTimer = TimePoll;
      if (PotentiometerPwm && !laststop) {
        DD("Poll", newTimer);
        DV(laststop);
        _pinDriverState = Step;
        laststop = PotentiometerPwm;
        break;
      }
      if (!PotentiometerPwm && laststop) {  //&& laststop
        _pinDriverState = Stop;
      }
      setStatePin(0);
      laststop = PotentiometerPwm;
      break;
    case Step:
      {
        // без датчиковый режим     newTimer = _pinDriverHalfPeriodReset * 300 / (255 - PotentiometerPwm) ; //_pinDriverHalfPeriod;
        _pinDriverState = Poll;
        newTimer = _pinDriverHalfPeriodReset;
        if (PotentiometerPwm) {
          if (!setStatePin(0)) {
            //DD("Step__", newTimer);
            _pinDriverState = Step;
            doStep(true);
          }
        } else
          DD("Step->poll", newTimer);
      }
      break;
    case Pause:
      newTimer = TimePause;
      _pinDriverState = Stop;
      break;
    case LastState:
      _pinDriverState = Stop;
      break;
    default:
      D("ERROR");
      printLeft(0x0E);
      _pinDriverState = Idle;
      return 0;
      break;
  }
  return (ulong)newTimer;
}

////////////////////////////////////////////////////////////////////
//
inline void doStep(bool set) {

  doStep1(set);
  doStep2(set);
}


const int StepNumber = 12;
////////////////////////////////////////////////////////////////////
//
inline void swing() {
  static const int steps1[StepNumber] = { 1, 2, 0, 1, 2, 1, 0, 1, 2, 1, 0, 2 };
  static const int steps2[StepNumber] = { 0, 2, 1, 2, 0, 1, 2, 1, 0, 2, 1, 2 };
  static uint curStep(0);
  static ulong lastMillisTimer(0);
  static ulong deltaMillis{ 0 };
  static ulong latency(0);
  static bool period(false);


  ulong currMills(micros());
  ulong delta = currMills - lastMillisTimer;

  if (delta > latency) {
    period = !period;
    if (period) {
      _StatePin1 = steps1[curStep] == 2 ? 0 : 1;
      _StatePin2 = steps2[curStep] == 2 ? 0 : 1;

      doStep1(steps1[curStep]);
      doStep2(steps2[curStep]);

      latency = 300000;
      if (++curStep >= StepNumber)
        curStep = 0;
    } else {
      doStep(false);
      latency = 100000;
    }
    DD(_StatePin1, _StatePin2);
    printLeft(curStep);
    DD(curStep, PotentiometerPwm);
    lastMillisTimer = currMills;
  }

  //   if (hall != lastHall && result) {
  //     lastHall = hall;
  //     D(delta);
  //   } else {
  //     if (delta > deltaMillis[hall])
  //       deltaMillis[hall] = delta;
  //     D("---");
  //     DD(delta, hall);
  //     DD(hallState, hallState2);

  //     return false;
  //   }
  //   // // if (deltaMillis[hall] > 1000 && delta < 30000) {
  // //   DD("reurn",hall);
  // //   DV(delta);
  // //   return false;
  // // }
  // if (delta > deltaMillis[hall])
  //   deltaMillis[hall] = delta;
  // DD(delta, hall);
  // DD(hallState, hallState2);
  // }
}
#define THRESHOLD 2000
////////////////////////////////////////////////////////////////////
//
inline void doStep1(bool set) {
  static volatile int Mutex(0);
  static volatile int MutexCount(0);

  if (Mutex) {
    MutexCount++;
    D("Mutex Mutex Mutex");
    printLeft(MutexCount % 10);
    goto end;
  }
  ++Mutex;
  int lev1(LOW);
  int lev2(LOW);
  if (set) {
    lev1 = _StatePin1 == 0 ? HIGH : LOW;
    lev2 = _StatePin1 == 1 ? HIGH : LOW;
  }

  if ((lev1 == LOW && LOW == lev2)) {
    D("1 STOP STOP STOP");
  }

  // защита от сгорания
  if ((lev1 == lev2 && lev1 == HIGH)) {
    D("ERROR");
    printLeft(0x0E);
    goto end;
  }
  byte pwm(PotentiometerPwm);
  //Для начала выключим , что надо выключить
  if (lev1 == LOW) {
    clDigitalWrite(HBridge2L, LOW);
    analogWrite(HBridge1H, 0);
  }
  if (lev2 == LOW) {
    clDigitalWrite(HBridge1L, LOW);
    analogWrite(HBridge2H, 0);
  }
  if (!pwm) goto end;

  // остатки тока пусть стекут на протвоположной фазе , зависимомсть должна быть по току ,а не обороты и курка газа
  if (pwm > THRESHOLD) {
    analogWrite(HBridge3H, 0);
    analogWrite(HBridge4H, 0);
  }

  if (lev1 == HIGH) {
    analogWrite(HBridge1H, pwm);
    clDigitalWrite(HBridge2L, lev1);
  }
  if (lev2 == HIGH) {
    analogWrite(HBridge2H, pwm);
    clDigitalWrite(HBridge1L, lev2);
  }
end:
  Mutex = 0;
}
////////////////////////////////////////////////////////////////////
//
inline void doStep2(bool set) {
  static volatile int Mutex(0);
  static volatile int MutexCount(0);

  if (Mutex) {
    MutexCount++;
    D("Mutex Mutex Mutex");
    printLeft(MutexCount % 10);
    goto end;
  }
  ++Mutex;

  int lev3(LOW);
  int lev4(LOW);
  if (set) {
    lev3 = _StatePin2 == 0 ? HIGH : LOW;
    lev4 = _StatePin2 == 1 ? HIGH : LOW;
  }
  if ((LOW == lev3 && LOW == lev4)) {
    D("2 STOP STOP STOP");
  }

  // защита от сгорания
  if ((lev3 == lev4 && lev3 == HIGH)) {
    D("ERROR");
    printLeft(0x0E);
    goto end;
  }
  byte pwm(PotentiometerPwm);
  //Для начала выключим , что надо выключить
  if (lev3 == LOW) {
    clDigitalWrite(HBridge4L, LOW);
    analogWrite(HBridge3H, 0);
  }
  if (lev4 == LOW) {
    clDigitalWrite(HBridge3L, LOW);
    analogWrite(HBridge4H, 0);
  }
  if (!pwm) goto end;

  // остатки тока пусть стекут на протвоположной фазе , зависимомсть должна быть по току ,а не обороты и курка газа
  if (pwm > THRESHOLD) {
    analogWrite(HBridge1H, 0);
    analogWrite(HBridge2H, 0);
  }

  if (lev3 == HIGH) {
    analogWrite(HBridge3H, pwm);
    clDigitalWrite(HBridge4L, lev3);
  }
  if (lev4 == HIGH) {
    analogWrite(HBridge4H, pwm);
    clDigitalWrite(HBridge3L, lev4);
  }

end:
  Mutex = 0;
}

////////////////////////////////////////////////////////////////////
//
inline void hallSensorChange() {
  setStatePin(1);
}
inline void hallSensorChange2() {
  setStatePin(2);
}
////////////////////////////////////////////////////////////////////
//
inline int rpm() {
  static ulong _periodTime = 0L;
  static int outCount = 0;
  static ulong turnTime = 0L;
  static int prd(0);

  ulong timeIn = micros();
  ulong period = (timeIn - _periodTime);
  _periodTime = timeIn;
  turnTime += period;
  if (++outCount > 12) {
    //int prd = (9765 / (period >> 10));
    prd = (60 * 1000000 / turnTime);
    //    DD("prd", prd);
    //printDec(prd);
    _decToPrint = prd;
    outCount = 0;
    turnTime = 0;
  }
  return prd;
}

///////////////////////////////////////
int _timer = 0;
ulong _lastMillisTimer = 0;
ulong timerWatch() {
  ulong delta = micros() - _lastMillisTimer;
  _lastMillisTimer = micros();
  printDec(_timer);
  //printLeft( byte(_timer) % 10);
  _timer++;

  return (ulong)0;
}
////////////////////////////////////////////////////////////////////
//
#define NullMesure 490
uint mVoltageReferenceHalf = 2100;
uint mV_A = 40;
uint ma_mv = 25;

ulong currienMesure() {
  static int i(0);
  static ulong sV(0);
  int V = analogRead(CurrentSensor);
  sV += V;

  if (++i >= 8) {
    //DD(V, sV);
    sV >>= 3;
    //DV(sV);
    //sV=sV/20;
    uint A = sV - NullMesure;
    int aaa = (ulong)(mVoltageReferenceHalf * ma_mv * A) / (NullMesure * 10);
    //DV(aaa);
    //printDec(aaa);
    sV = i = 0;
  }
  return (ulong)0;
}
////////////////////////////////////////////////////////////////////
inline int getPotentiometerNil() {
  ulong sV(0);
  for (int i = 0; i < 16; ++i) {
    //    int V = clAnalogRead(P_POT);
    int V = analogRead(P_POT);
    sV += V;
  }
  sV >>= 4;
  DD("potentiometerNil", sV);
  return sV;
}
////////////////////////////////////////////////////////////////////
#define Coolness 20
ulong potentiometer() {
  static int i(0);
  static ulong sV(0);
  //  int V = clAnalogRead(P_POT);
  int V = analogRead(P_POT);
  sV += V;
  if (++i >= 4) {
    int potentiometer(PotentiometerPwm);
    //DD(V, sV);
    sV >>= 2;
    potentiometer = sV - PotentiometerNil;
    potentiometer = potentiometer < 0 ? 0 : potentiometer;
    //DD(">", potentiometer);
    potentiometer >>= 1;

    //    potentiometer = potentiometer < Coolness ? potentiometer << 1 :  potentiometer + Coolness ;
    potentiometer = potentiometer > MAX_PWM ? MAX_PWM : potentiometer;
    //if (potentiometer) D(potentiometer);
    sV = i = 0;
    PotentiometerPwm = potentiometer;
  }
  return (ulong)0;
}

////////////////////////////////////////////////////////////////////
char _nunToPrint[4] = { 0, 0, 0, 0 };
////////////////////////////////////////////////////////////////////
inline void printLeft(byte num) {
  _nunToPrint[0] = num;
  printNum(0);
}
////////////////////////////////////////////////////////////////////
inline void printSecond(byte num) {
  _nunToPrint[1] = num;
  printNum(1);
}
////////////////////////////////////////////////////////////////////
inline void printThird(byte num) {
  _nunToPrint[2] = num;
  printNum(2);
}
////////////////////////////////////////////////////////////////////
inline void printDec(int out) {
  int num = out;
  for (int i = 3; i >= 0; --i) {
    _nunToPrint[i] = num % 10;
    num = num / 10;
  }
  printNum(-1);
}

////////////////////////////////////////////////////////////////////
void printHex(int num) {
  for (int i = 0; i < 4; i++) {
    _nunToPrint[3 - i] = (char)(num & 0x0F);
    num = num >> 4;
  }
  printNum(-1);
}

////////////////////////////////////////////////////////////////////
void printNum(int pos) {
#ifdef UseTM1637
  if (pos < 0)
    for (int i = 0; i < 4; ++i)
      tm1637.display(i, _nunToPrint[i]);
  else
    tm1637.display(pos, _nunToPrint[pos]);
#endif
}
////////////////////////////////////////////////////////////////////
ulong showDec() {
  static int dec(0);
  if (_decToPrint == dec)
    return (ulong)0;
  dec = _decToPrint;
  printDec(dec);
  return (ulong)0;
}
////////////////////////////////////////////////////////////////////
byte _currientDigit = 0;
ulong showNum() {
#ifdef UseTM1637

  //  for (int i = 0; i < 4; ++i)
  //    tm1637.display(i, _nunToPrint[i]);
  tm1637.display(_currientDigit, _nunToPrint[_currientDigit]);
  _currientDigit = _currientDigit == 3 ? 0 : _currientDigit + 1;
#endif

  return (ulong)0;
}
////////////////////////////////////////////////////////////////////
void PRINT(const char *format, ...) {
  char buffer[256];
  va_list args;
  va_start(args, format);
  vsnprintf(buffer, 256, format, args);
  va_end(args);
  D(buffer);
}



////////////////////////////////////////////////////////////////////////////
